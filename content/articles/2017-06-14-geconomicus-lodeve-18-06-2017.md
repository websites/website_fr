Title: Ğeconomicus à Lodève le 18/06/2017
Date: 2017-06-14 13:28
Category: Évènements
Tags: Ğeconomicus, jeu, monnaie libre
Slug: geconomicus-lodeve-18-06-2017
Authors: cgeek
Thumbnail: images/geconomicus.png
Summary: Un Ğeconomicus est organisé à Lodève le 18/06/2017.

Un Ğeconomicus est organisé à Lodève le 18/06/2017.

[![Ğeconomicus à la Compagnie des Jeux, de 10h à 18h]({static}/images/evenements/geconomicus-lodeve.jpg)](https://www.monnaielibreoccitanie.org/event/jeu-geconomicus-a-lodeve/)

Plus d'informations sur [monnaielibreoccitanie.org](https://www.monnaielibreoccitanie.org/event/jeu-geconomicus-a-lodeve/)