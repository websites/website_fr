Title: Conférence à Lodève le 14/06/2017
Date: 2017-06-14 13:22
Category: Évènements
Tags: Conférence, monnaie libre
Slug: conference-lodeve-14-06-2017
Authors: cgeek
Thumbnail: images/calendar.svg
Summary: Une conférence animée par Eloïs aura lieu mercredi 14 juin 2017 22h00 à Lodève.

Une conférence animée par Eloïs aura lieu mercredi 14 juin 2017, à 22h00 à Lodève :

[![Conférence au C.L.A.P, 9 avenue DENFERT, à Lodève]({static}/images/evenements/conf-elois-lodeve.png)](https://www.monnaielibreoccitanie.org/event/1ere-conference-monnaie-libre-a-lodeve/)
