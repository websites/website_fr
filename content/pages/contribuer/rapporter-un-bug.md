Title: Rapporter un bug
Order: 9
Date: 2017-03-27
Slug: rapporter-un-bug
Authors: cgeek

# Rapporter un bug

Il se peut que vous détectiez un bug sur l'une des applications de Duniter ! Si c'est le cas, **merci de rédiger un rapport de bug**, comme expliqué ci-après.

[TOC]

## Rapporter un bug via les plateformes dédiées

C'est la méthode à privilégier. *Vraiment, nous préférons celle-ci* : **c'est notre liste officielle de bugs**.

Vous devez d'abord [créer un compte GitHub](https://github.com/join), ce qui vous permettra par la suite de [créer un compte sur notre GitLab](https://git.duniter.org/users/sign_in).

Une fois créé votre compte GitHub et éventuellement GitLab, allez sur le dépôt correspondant au projet pour lequel vous souhaitez rapporter un bug :

* [Duniter](https://git.duniter.org/nodes/typescript/duniter/issues)
* [Sakia](https://github.com/duniter/sakia/issues)
* [Cesium](https://git.duniter.org/clients/cesium/cesium/issues)
* [Silkaj](https://github.com/duniter/silkaj/issues)

Puis mettez un titre, et un descriptif de l'erreur rencontrée :

![Créer un ticket sur le GitLab]({static}/images/contribuer/ticket_gitlab.png)

**/!\ N'oubliez pas de valider en cliquant sur le bouton « Submit issue » !**

## Rapporter un bug via le forum

Si *vraiment* vous vous sentez en difficulté pour saisir le ticket sur GitHub (c'est pourtant une bonne occasion d'apprendre !), vous pouvez également créer un sujet sur le forum Duniter :

[forum.duniter.org/](http://forum.duniter.org/)

Veillez bien à :

* donner un titre
* donner un descriptif
* sélectionner la catégorie « support »

[![Créer un ticket sur le forum]({static}/images/contribuer/ticket_forum.png)](http://forum.duniter.org/)

Puis validez. 

Quelqu'un vous répondra, puis il *nous faudra* ensuite *saisir votre problème* sur Gitlab. Mais bon, vous rapportez un bug, c'est le plus important !
