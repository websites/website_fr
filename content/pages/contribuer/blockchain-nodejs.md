Title: Contribuer à Duniter
Order: 9
Date: 2017-04-08
Slug: blockchain-nodejs
Authors: cgeek
Breadcrumb: Cœur Node.js

# Comment contribuer à Duniter

## Concepts généraux

* [Preuve de travail]({filename}blockchain-nodejs/preuve-de-travail.md)
* [Architecture]({filename}blockchain-nodejs/architecture-duniter.md)
* [Réflexions techniques autour de Duniter et de sa philosophie \[archives\]]({filename}blockchain-nodejs/archives.md)

## Implémentation

* [Tutoriel de développement]({filename}blockchain-nodejs/tutoriel-dev.md)
* [Fonctionnement des branches du dépot git duniter]({filename}blockchain-nodejs/git-branches.md)
* [Fuites mémoire]({filename}blockchain-nodejs/fuites-memoire.md)
* [Les modules C/C++]({filename}blockchain-nodejs/les-modules-c-cpp.md)
* [Guide du validateur](https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/validator-guide.md)
* [Livraisons]({filename}blockchain-nodejs/livraisons.md)
* [Livraisons (ancien)]({filename}blockchain-nodejs/livraisons-old.md)
* [Du Rust dans NodeJs graçe a Neon]({filename}blockchain-nodejs/binding-node-rust.md)

