Title: Licences du site
Order: 9
Date: 2017-03-27
Slug: licences-du-site
Authors: cgeek

# Licences du site

## Contenus

L'intégralité de ce site est sous licence [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) sauf mention contraire explicite.

## Code source

Le code source du site est par ailleurs intégralement disponible sur le dépôt [git.duniter.org/websites/website_fr](https://git.duniter.org/websites/website_fr) avec les instructions permettant de le reproduire.

## Images utilisées sur ce site

Certains logos ou images sont la production d'autres personnes, vous trouverez leur origine sur la page [Crédits]({filename}credits.md).