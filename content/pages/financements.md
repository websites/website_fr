Title: Comment est financé Duniter
Order: 10
Date: 2018-01-21
Slug: financements
Authors: cgeek

# Comment est financé le projet Duniter

Duniter/Ğ1 (le logiciel et sa monnaie) sont des projets long terme dont les développements 
ont débuté en été 2013 et sont perpétuels : chaque jour de nouvelles idées apparaissent, 
qu'il s'agisse d'améliorations en termes de sécurité, d'ergonomie ou de fonctionnalités nouvelles.

**Développer prend du temps et demande un travail non-négligeable.**

Afin de pérenniser ce projet, financer ces développements en rémunérant leurs auteurs est primordial. 

## Qui finance Duniter ?

Pour pérenniser le projet Duniter, nous comptons sur les **dons** des utilisateurs de la monnaie.

Bien évidemment, **ces dons se font en Ğ1**. 

Ainsi les développements sont financés dans la monnaie qu'ils concrétisent.

## Comment vous pouvez aider à financer Duniter

Il vous faut d'abord des Ğ1. Si vous ne savez pas comment faire, consultez la page [Obtenir des Ğ1]({filename}monnaie-libre-g1/obtenir-des-g1.md).

**Pour le moment**, nous nous reposons sur un compte central qui réceptionne les dons et dont la clé est :

```
78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8
```

Ce compte est géré par moi-même (cgeek), et l'attribution de sommes pour chaque développement sera décidée cas par cas, en fonction de ce que chacun estime juste pour les travaux qu'il entreprend.

Toutes les décisions d'attribution de financement seront publiquement consultables sur cette page.

**Dans le futur**, nous espérons disposer d'outils évitant que les dons ne soient gérés que par une seule personne.

## Où partent les dons ?

Pour vérifier les versements, vous pouvez consulter 
[les mouvements du portefeuille](https://g1.duniter.fr/#/app/wot/78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8/).

Les contributeur indemnisés à chaque cycle sont listés dans [la catégorie Financements]({category}financement) du blog.

Les dernières transactions effectuées sont listées sur le forum technique dans le sujet 
[Rémunération des contributeurs au projet Duniter](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/). 
Ci-dessous, vous trouverez, pour chaque mois, le lien d'accès direct au message idoine, suivi de l'indémnisation reçu par 
chaque contributeur :


| Date | Indemnisation |
| --- | --- |
| [janvier 2020](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/71) | 20 DU<sub>Ğ1</sub> à 19 contributeurs |
| [décembre 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/54) | 20 DU<sub>Ğ1</sub> à 19 contributeurs |
| [novembre 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/52) | 20 DU<sub>Ğ1</sub> à 22 contributeurs |
| [octobtre 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/51) | 20 DU<sub>Ğ1</sub> à 22 contributeurs |
| [septembre 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/49) | 20 DU<sub>Ğ1</sub> à 22 contributeurs |
| [août 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/45) | 20 DU<sub>Ğ1</sub> à 18 contributeurs |
| [juillet 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/36) | 20 DU<sub>Ğ1</sub> à 18 contributeurs |
| [juin 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/22) | 20 DU<sub>Ğ1</sub> à 18 contributeurs |
| [mai 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/20) | 20 DU<sub>Ğ1</sub> à 16 contributeurs |
| [avril 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/19) | 20 DU<sub>Ğ1</sub> à 16 contributeurs |
| [mars 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/17) | 20 DU<sub>Ğ1</sub> à 16 contributeurs |
| [février 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/16) | 20 DU<sub>Ğ1</sub> à 15 contributeurs |
| [janvier 2019](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/15) | 20 DU<sub>Ğ1</sub> à 15 contributeurs |
| [décembre 2018](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/12) | 20 DU<sub>Ğ1</sub> à 15 contributeurs |
| [novembre 2018](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/7) | 20 DU<sub>Ğ1</sub> à 13 contributeurs |
| [octobre 2018](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/3) | 200 Ğ1 à 13 contributeurs |
| septembre 2018 |  |
| [août 2018](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995/2) | 200 Ğ1 à 13 contributeurs |
| [juillet 2018](https://forum.duniter.org/t/remuneration-des-contributeurs-au-projet-duniter/4995) | 200 Ğ1 à 13 contributeurs |
| [juin 2018](https://forum.duniter.org/t/silkaj-installation-virements-automatiques-et-multi-destinataires/4836/6) | 200 Ğ1 à 13 contributeurs |
| mai 2018 |  |
| avril 2018 | 100,00 Ğ1 à 10 contributeurs |
| mars 2018 | 100,00 Ğ1 à 10 contributeurs |
