Title: Comment obtenir des Ğ1
Order: 2
Date: 2017-03-27
Tags: presentation, théorie, trm, blockchain
Slug: obtenir-des-g1
Authors: cgeek

# Comment obtenir des Ğ1

## Méthode n°1 : en échanger

Que vous soyez un individu, une association, entreprise ou un robot, vous pouvez tout à fait vous procurer des unités de monnaie Ğ1.

Au moment de l'écriture de ces lignes, le 2 janvier 2020, il existe plus de **12 millions de Ğ1** en circulation (soit environ **5 000 Ğ1 par membre certifié**).

Pour vous procurer des unités de monnaie Ğ1 il faut créer un portefeuille Ğ1, puis trouvez une personne prête à faire un échange.

Oui mais un échange de quoi ?

D'unités Ğ1 (pour celui qui en possède) contre un bien ou service (pour celui qui en veut).

En 2020, la plateforme la plus populaire pour échanger en des biens et services en Ğ1 est [ğchange](https://www.gchange.fr/#/app/home).

Une autre solution possible **est d'utiliser les réseaux sociaux tels [diaspora*](https://framasphere.org)** pour y poster vos annonces. Avec le hashtag #Ğ1, vous pourriez attirer quelques intéressés.

## Méthode n°2 : en produire

Mais si vous êtes un individu ne produisant pas encore d'unités Ğ1, sachez que *vous pourriez en produire vous-même*. C'est une propriété fondamentale de Ğ1 : tout individu peut produire sa part de monnaie sous réserve de devenir membre de la toile de confiance Ğ1.

Devenir membre n'est ni anodin ni instantané :

Cette étape fait intervenir des humains et s'effectue donc à temps humain. Devenir membre consiste à être reconnu par des personnes déjà membres de la toile ; c'est ce que l'on appele « être certifié ». 

Si vous recevez suffisamment de certifications (cinq minimum) et en bonne qualité (notion de distance), alors ces certifications seront agrégées à toutes celles déjà existantes et viendront augmenter la toile de confiance.

Si donc vous souhaitez poursuivre et devenir membre de la toile, prenez tout d'abord connaissances de **[la licence de la monnaie Ğ1]({filename}licence-g1.md)**. Vous pouvez aussi la télécharger pour en conserver une copie.

### Comment connaître l'état de sa demande d'adhésion

Il ne suffit pas toujours d'obtenir cinq certification pour devenir immédiatement certifié. 

Si certaines des personnes qui vous certifient ont émis d'autres certifications avant d'émettre celle vous concernant, vous entrez dans une file d'attente.

Vous pouvez voir cette file d'attente, et connaître les certifications reçues sur [la page « willMembers » de ğ1-monit](https://g1-monit.elois.org/willMembers?lg=fr&hideIdtyWithZeroCert=yes). Dans cette page, les membres qui sont le plus en haut ont le plus de chances de devenir membre à plus ou moins court terme.

### Une fois membre...

Quand vous serez vous-même membre, vous pourrez alors [certifier de nouveaux membres]({filename}../toile-de-confiance/certifier-de-nouveaux-membres.md).

## Comment se familiariser avec les monnaies libres

Nous vous conseillons d'abord de vous essayer au *réseau de test* et sa monnaie ĞTest en suivant [les tutoriels de l'association Monnaie Libre Occitanie](https://www.monnaielibreoccitanie.org/2017/01/24/nouvelle-monnaie-de-test-gtest/). 

Ce faisant, vous ne prenez aucun risque : il n'y a aucun enjeu dans cette monnaie, nous l'avons conçue pour cela.

Une fois suffisamment aguerri avec ĞTest, vous pourrez alors tenter de passer à Ğ1. Vous pourrez commencer par *vous rapprocher de personnes déjà membres de Ğ1 et vous connaissant*, leurs certifications étant nécessaires pour devenir membre.
